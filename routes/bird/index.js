var express = require('express');
var router = express.Router();

router.use(function (req, res, next) {
    console.log('Time: ', Date.now());
    next();
});

var setRequestTime = function (req, res, next) {
    req.requestTime = Date.now();
    next();
};

router.use(setRequestTime);

router.get('/', function (req, res, next) {
    res.render('bird/index', { title: 'Bird Home', requestTime: req.requestTime });
});
router.get('/about', function (req, res, next) {
    res.send('respond with a resource');
});

module.exports = router;
